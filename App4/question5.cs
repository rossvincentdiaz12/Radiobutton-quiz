﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Views.View;

namespace App4
{
    [Activity(Label = "question5")]
    public class question5 : Activity, IOnClickListener
    {
        public RadioButton q1, q2, q3, q4;

        public void OnClick(View v)
        {
            //action when login button is click
            if (v.Id == Resource.Id.radioButton4)
            {

                question1.score = question1.score + 1;
                StartActivity(new Android.Content.Intent(this, typeof(question6)));
                Toast.MakeText(this, "Question 6", ToastLength.Short).Show();
                Finish();

            }
            //action when forgotpassword button is click
            else if (v.Id == Resource.Id.radioButton2)
            {

                //closing the app
                StartActivity(new Android.Content.Intent(this, typeof(question6)));
                Toast.MakeText(this, "Question 6", ToastLength.Short).Show();
                Finish();
            }
            else if (v.Id == Resource.Id.radioButton3)
            {

                //closing the app
                StartActivity(new Android.Content.Intent(this, typeof(question6)));
                Toast.MakeText(this, "Question 6", ToastLength.Short).Show();
                Finish();
            }
            else if (v.Id == Resource.Id.radioButton1)
            {

                //closing the app
                StartActivity(new Android.Content.Intent(this, typeof(question6)));
                Toast.MakeText(this, "Question 6", ToastLength.Short).Show();
                Finish();
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.questionfive);

            q1 = FindViewById<RadioButton>(Resource.Id.radioButton1);
            q2 = FindViewById<RadioButton>(Resource.Id.radioButton2);
            q3 = FindViewById<RadioButton>(Resource.Id.radioButton3);
            q4 = FindViewById<RadioButton>(Resource.Id.radioButton4);

            q1.SetOnClickListener(this);
            q2.SetOnClickListener(this);
            q3.SetOnClickListener(this);
            q4.SetOnClickListener(this);
        }
    }
}