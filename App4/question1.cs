﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Views.View;


namespace App4
{
    [Activity(Label = "question1")]
    public class question1 : Activity, IOnClickListener
    {
        public static int score;

        public RadioButton q1, q2, q3, q4;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.questionone);
            // Create your application here
            q1 = FindViewById<RadioButton>(Resource.Id.radioButton1);
            q2 = FindViewById<RadioButton>(Resource.Id.radioButton2);
            q3 = FindViewById<RadioButton>(Resource.Id.radioButton3);
            q4 = FindViewById<RadioButton>(Resource.Id.radioButton4);

            q1.SetOnClickListener(this);
            q2.SetOnClickListener(this);
            q3.SetOnClickListener(this);
            q4.SetOnClickListener(this);
        }

        public void OnClick(View v)
        {
            //action when login button is click
            if (v.Id == Resource.Id.radioButton1)
            {

                score = score + 1;
                Toast.MakeText(this, "Question 2", ToastLength.Short).Show();
                StartActivity(new Android.Content.Intent(this, typeof(question2)));
                Finish();

            }
            //action when forgotpassword button is click
            else if (v.Id == Resource.Id.radioButton2)
            {

                //closing the app
                StartActivity(new Android.Content.Intent(this, typeof(question2)));
                Toast.MakeText(this, "Question 2", ToastLength.Short).Show();
                Finish();
            }
            else if (v.Id == Resource.Id.radioButton3)
            {

                //closing the app
                StartActivity(new Android.Content.Intent(this, typeof(question2)));
                Toast.MakeText(this, "Question 2", ToastLength.Short).Show();
                Finish();
            }
            else if (v.Id == Resource.Id.radioButton4)
            {

                //closing the app
                StartActivity(new Android.Content.Intent(this, typeof(question2)));
                Toast.MakeText(this, "Question 2", ToastLength.Short).Show();
                Finish();
            }
        }
    }
}