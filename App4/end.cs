﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Views.View;

namespace App4
{
    [Activity(Label = "end")]
    public class end : Activity, IOnClickListener
    {
        public Button btnmenu;
        public TextView txtscore;

        public void OnClick(View v)
        {
            //action when login button is click
            if (v.Id == Resource.Id.btnmenu)
            {

                StartActivity(new Android.Content.Intent(this, typeof(MainActivity)));
                Toast.MakeText(this, "Quiz Start", ToastLength.Short).Show();
                question1.score = 0;
                Finish();

            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.end);

            btnmenu = FindViewById<Button>(Resource.Id.btnmenu);
            txtscore = FindViewById<TextView>(Resource.Id.txtScore);

            btnmenu.SetOnClickListener(this);

            txtscore.Text = question1.score.ToString();
        }
    }
}